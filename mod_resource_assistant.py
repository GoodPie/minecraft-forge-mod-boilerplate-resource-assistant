def create_item_file(name, res):
    """
    Creates the appropriate resource file for a regular item

    :param name: Name of the item to create
    :param res:  Location of resources file for mod
    :return:
    """
    template = ('{{'
      '\n  "parent": "item/generated",'
      '\n  "textures": {{'
      '\n    "layer0": "gems_mod:{0}"'
      '\n  }}'
      '\n}}').format(name)

    item_folder = res + "/models/item/"
    write_json_file(item_folder + name, template)

def create_item_tool_file(name, res):
    """
    Creates the appropriate resource file for a new tool

    :param name: Name of the tool to create
    :param res:  Location of resources file for mod
    :return:     None
    """
    template = ('{{'
      '\n  "parent": "item/handheld",'
      '\n  "textures": {{'
      '\n    "layer0": "gems_mod:items/{0}"'
      '\n  }}'
      '\n}}').format(name)

    item_folder = res + "/models/item/"
    write_json_file(item_folder + name, template)

def create_item_block_file(name, res):
    """
    Creates the appropriate resource file for a new block item

    :param name: Name of the block item to create
    :param res:  Location of resource folder for the mod
    :return:
    """
    template = ('{{'
    '\n  "parent": "gems_mod:block/{0}"'
    '\n}}').format(name)

    item_folder = res + "/models/item/"
    write_json_file(item_folder + name, template)

def create_block_file(name, res):
    """
    Creates the appropriate resource file for a new block

    :param name: Name of the block to create
    :param res:  Location of the resource file for the mod
    :return:
    """
    template = ('{{'
      '\n  "parent\": "block/cube_all",'
      '\n  "textures": {{'
      '\n    "all": "gems_mod:blocks/{0}"'
      '\n  }}'
      '\n}}').format(name)

    block_folder = res + "/models/block/"
    write_json_file(block_folder + name, template)

    # Block also needs to be an item
    create_item_block_file(name, res)

def create_block_state(name, res):
    """
    Creates a block state resource file, which is used for creating a new block

    :param name: Name of the block to create
    :param res:  Location of the resource folder
    :return:
    """
    template =  ('{{'
      '  "variants": {{'
      '    "normal": {{"model": "{0}"}}'
      '  }}'
      '}}').format(name)

    blockstate_folder = res + "/blockstates/"
    write_json_file(blockstate_folder + name, template)

def write_json_file(file, content):
    """
    Writes content to a new JSON file

    :param file:    Name of file (without extension) to create
    :param content: Content of the new JSON file
    :return:
    """
    file = open(file + ".json", "w+")
    file.write(content)
    file.close()

resources_location = input("Please input your resources folder location: ").replace("\\", "/")

running = True
while running:

    # The options available at the moment are tools, items and blocks
    valid_answer = False
    while not valid_answer:
        choice = input("Would you like to create an [I]tem, [B]lock or [T]ool: ")
        choice = choice.lower()

        # The name of the item we are creating in all lowercase
        item_name = input("Please enter your item name (no spaces, all lowercase): ")
        item_name = item_name.replace(" ", "_").lower()
        
        if choice in ["i", "item"]:
            # Create an item
            print("Creating new item: " + item_name)
            create_item_file(item_name, resources_location)
        elif choice in ["b", "block"]:
            # Create a block
            # Need to register item and block
            print("Creating new block: " + item_name)
            create_item_block_file(item_name, resources_location)
            create_block_state(item_name, resources_location)
        elif choice in ["t", "tool"]:
            # Create a tool
            print("Creating new tool: " + item_name)
            create_item_tool_file(item_name, resources_location)
        elif choice in ["e", "q", "exit", "quit"]:
            # Exit the program
            valid_answer = True
            running = False
            print("Exiting program...")
        else:
            "Invalid input. Try again."

            
